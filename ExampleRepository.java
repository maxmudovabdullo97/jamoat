package com.example.popularnews.database;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.popularnews.model.Example;

import java.util.List;

public class ExampleRepository {

    private ExampleDao exampleDao;
    private LiveData<List<Example>> allExamples;

    public ExampleRepository(Application application) {
        ExampleRoomDatabase database = ExampleRoomDatabase.getInstance(application);
        exampleDao = database.exampleDao();
        allExamples = exampleDao.getAllExamples();
    }

    public void insert(Example example) {
        new InsertExampleAsyncTask(exampleDao).execute(example);

    }

    public void update(Example example) {
        new UpdateExampleAsynTask(exampleDao).execute(example);
    }

    public void delete(Example example) {
        new DeleteExampleAsynTask(exampleDao).execute(example);
    }

    public void deleteAllExamples() {
        new DeleteAllExampleAsynTask(exampleDao).execute();
    }

    public LiveData<List<Example>> getAllExamples() {
        return allExamples;
    }

    private static class InsertExampleAsyncTask extends AsyncTask<Example, Void, Void>{
        private ExampleDao exampleDao;

        private InsertExampleAsyncTask(ExampleDao exampleDao){
            this.exampleDao = exampleDao;
        }
        @Override
        protected Void doInBackground(Example... examples) {
            exampleDao.insert(examples[0]);
            return null;
        }
    }

    private static class DeleteExampleAsynTask extends AsyncTask<Example, Void, Void> {
        private ExampleDao exampleDao;

        private DeleteExampleAsynTask(ExampleDao exampleDao) {
            this.exampleDao = exampleDao;

        }

        @Override
        protected Void doInBackground(Example... example) {
            exampleDao.delete(example[0]);
            return null;
        }
    }


    private static class DeleteAllExampleAsynTask extends AsyncTask<Void, Void, Void> {
        private ExampleDao exampleDao;

        private DeleteAllExampleAsynTask(ExampleDao exampleDao) {
            this.exampleDao = exampleDao;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            exampleDao.deleteAllItems();
            return null;
        }
    }

    private static class UpdateExampleAsynTask extends AsyncTask<Example, Void, Void> {
        private ExampleDao exampleDao;

        private UpdateExampleAsynTask(ExampleDao exampleDao) {
            this.exampleDao = exampleDao;

        }

        @Override
        protected Void doInBackground(Example... example) {
            exampleDao.deleteAllItems();
            return null;
        }
    }


}
