package com.example.popularnews.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.popularnews.model.Example;

import java.util.List;

public class ExampleViewModel extends AndroidViewModel {
    private ExampleRepository repository;
    private LiveData<List<Example>> allExamles;


    public ExampleViewModel(@NonNull Application application) {
        super(application);
        repository = new ExampleRepository(application);
        allExamles = repository.getAllExamples();
    }
public void insert(Example example){
        repository.insert(example);
}

public void update(Example example){
        repository.update(example);
}
public void delete(Example example){
repository.delete(example);
}

public void deleteAllExamples(){
        repository.deleteAllExamples();
}

public LiveData<List<Example>> getAllExamles(){
        return allExamles;
    }
}
