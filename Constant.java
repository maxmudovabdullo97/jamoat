/*
 * Copyright (c) 2015-2016 Filippo Engidashet. All Rights Reserved.
 * <p>
 *  Save to the extent permitted by law, you may not use, copy, modify,
 *  distribute or create derivative works of this material or any part
 *  of it without the prior written consent of Filippo Engidashet.
 *  <p>
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 */

package com.example.popularnews.helper;

/**
 * @author Filippo Engidashet
 * @version 1.0
 * @date today
 */
public class Constant {

    public static final class HTTPS {
      //  public static final String BASE_URL = "http://services.hanselandpetal.com";
        public static final String BASE_URL = "https://jamoatchilik.uz/api/";
    }

    public static final class DATABASE {

        public static final String DB_NAME = "flowers";
        public static final int DB_VERSION = 1;
        public static final String TABLE_NAME = "flower";

        public static final String DROP_QUERY = "DROP TABLE IF EXIST " + TABLE_NAME;

        public static final String GET_FLOWERS_QUERY = "SELECT * FROM " + TABLE_NAME;

        public static final String PRODUCT_ID = "Id";
        public static final String FIO = "fio";
        public static final String REGION_NAME = "REGION_NAME";
        public static final String CATEGORY_NAME = "CATEGORY_NAME";
        public static final String TAKLIF = "TAKLIF";
        public static final String LIKE = "LIKE";
        public static final String DISLIKE = "DISLIKE";
        public static final String TIME = "TIME";

        public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "" +
                "(" + PRODUCT_ID + " INTEGER PRIMARY KEY not null," +
                FIO + " TEXT not null," +
                REGION_NAME + " TEXT not null," +
                CATEGORY_NAME + " TEXT not null," +
                TAKLIF  + " TEXT not null," +
                LIKE + " INTEGER not null," +
                TIME + " DATETIME not null," +
                DISLIKE + " INTEGER not null)";
    }

    public static final class REFERENCE {
        public static final String FLOWER = Config.PACKAGE_NAME + "flower";
    }

    public static final class Config {
       // public static final String PACKAGE_NAME = "org.dalol.retrofit2_restapidemo.";
        public static final String PACKAGE_NAME = "com.example.popularnews.";
    }


}
