package com.example.popularnews.pojo;

import androidx.room.TypeConverter;

import com.example.popularnews.model.Example;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverter implements Serializable {

    private String categoryValuesString;

    @TypeConverter // note this annotation
    public String fromCategoryList(List<Example> categoryValues) {
        if (categoryValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Example>>() {
        }.getType();
        String json = gson.toJson(categoryValues, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<Example> toCategoryList(String CategoryString) {
        if (CategoryString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Example>>() {
        }.getType();
        List<Example> productCategoriesList = gson.fromJson(categoryValuesString, type);
        return productCategoriesList;
    }


}
