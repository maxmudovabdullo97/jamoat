package com.example.popularnews.pojo;

import androidx.room.TypeConverter;

import com.example.popularnews.model.Category;
import com.example.popularnews.model.Region;

public class DataTypeConverter {
    @TypeConverter
    public String convertCategoryToString(Category category) {
        return category.getName();
    }

    @TypeConverter
    public Category convertStringToCategory(String category) {
        return new Category();
    }


    @TypeConverter
    public String convertRegionToString(Region region) {
        return region.getName();
    }

    @TypeConverter
    public Region convertStringToRegion(String region) {
        return new Region();
    }
}