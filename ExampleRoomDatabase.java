package com.example.popularnews.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.popularnews.model.Example;

@Database(entities = {Example.class}, version = 1)
public abstract class ExampleRoomDatabase extends RoomDatabase {

    private  static ExampleRoomDatabase instance;



    public abstract ExampleDao exampleDao();


    public  static synchronized ExampleRoomDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    ExampleRoomDatabase.class, "items_table")
                     .fallbackToDestructiveMigration()
                     .allowMainThreadQueries()
                     .addCallback(roomCallback)
                     .build();
        }
    return instance;
    }


    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            new PopulateDbAsyncTask(instance).execute();

        }
    };
    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void>{
        private ExampleDao exampleDao;



        private PopulateDbAsyncTask(ExampleRoomDatabase db) {
            exampleDao = db.exampleDao();

        }


        @Override
        protected Void doInBackground(Void... voids) {
            exampleDao.insert(new Example("","", "", "","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            exampleDao.insert(new Example("", "", "","","","",""));
            return null;
        }
    }
}
